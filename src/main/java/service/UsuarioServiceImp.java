package service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.jackson.JacksonFeature;

import dto.Album;
import dto.Usuario;

public class UsuarioServiceImp implements UsuarioService {

	@Override
	public List<Usuario> getListUsuarios() {
		Client client = ClientBuilder.newClient();
		List<Usuario> listUsuario = new ArrayList<Usuario>();

		try {
			client = ClientBuilder.newClient().register(JacksonFeature.class);

//		WebTarget target = client.target("https://jsonplaceholder.typicode.com/users/").path("posts");
//		Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);

			Response resp = client.target("https://jsonplaceholder.typicode.com/users/").request()
					.accept(MediaType.APPLICATION_JSON).get();
			listUsuario = resp.readEntity(new GenericType<List<Usuario>>() {
			});
			for (int i = 0; i < listUsuario.size(); i++) {
				System.out.println(listUsuario.get(i).getName() + " - " + listUsuario.get(i).getEmail());

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return listUsuario;

	}

	@Override
	public List<Usuario> getListUsuarioAlbum() {
		List<Usuario> listaUsuario = new ArrayList<Usuario>();
		Client client = ClientBuilder.newClient();
		List<Album> listaAlbums = new ArrayList<Album>();
		try {
			client = ClientBuilder.newClient().register(JacksonFeature.class);
			Response resp = client.target("https://jsonplaceholder.typicode.com/users/").request()
					.accept(MediaType.APPLICATION_JSON).get();
			listaUsuario = resp.readEntity(new GenericType<List<Usuario>>() {
			});
			for (int i = 0; i < listaUsuario.size(); i++) {
				if (listaUsuario.get(i).getId() == 1) {
					listaAlbums = client.target("https://jsonplaceholder.typicode.com/albums").request()
							.accept(MediaType.APPLICATION_JSON).get().readEntity(new GenericType<List<Album>>() {
							});

					for (int j = 0; j < listaAlbums.size(); j++) {
						if (listaAlbums.get(j).getUserId().equals("1")) {
							System.out.println("id Usuario" + listaUsuario.get(i).getId() + " - nombre Usuario :"
									+ listaUsuario.get(i).getName() + " | album id" + listaAlbums.get(j).getId()
									+ " - album " + listaAlbums.get(j).getTitle());

						}

					}
				}
				if (listaUsuario.get(i).getId() == 2) {
					for (int j = 0; j < listaAlbums.size(); j++) {
						if (listaAlbums.get(j).getUserId().equals("2")) {
							System.out.println("id Usuario" + listaUsuario.get(i).getId() + " - nombre Usuario :"
									+ listaUsuario.get(i).getName() + " | album id" + listaAlbums.get(j).getId()
									+ " - album " + listaAlbums.get(j).getTitle());
						}
					}
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return listaUsuario;
	}

}
