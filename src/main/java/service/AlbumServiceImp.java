package service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dto.Album;
import dto.Foto;

public class AlbumServiceImp implements AlbumService {

	@Override
	public List<Album> getAlbumList() {
		boolean encontrado = true;
		List<Album> listAlbum = new ArrayList<Album>();
		Client client = ClientBuilder.newClient();

		try {
			Response resp = client.target("https://jsonplaceholder.typicode.com/albums").request()
					.accept(MediaType.APPLICATION_JSON).get();
			listAlbum = resp.readEntity(new GenericType<List<Album>>() {
			});
			for (int i = 0; i < listAlbum.size(); i++) {
				if (listAlbum.get(i).getUserId().equals("5")) {
					System.out.println(listAlbum.get(i).getId() + " - " + listAlbum.get(i).getUserId() + " - "
							+ listAlbum.get(i).getTitle());
				}
			}

			// listAlbum.stream().filter(list ->list.);
		} catch (Exception e) {
			System.out.println(e);
		}
		return listAlbum;
	}

	@Override
	public List<Album> getAlbum() {
		Client client = ClientBuilder.newClient();
		List<Album> listAlbum = new ArrayList<Album>();
		try {

			Response resp = client.target("https://jsonplaceholder.typicode.com/albums").request()
					.accept(MediaType.APPLICATION_JSON).get();
			listAlbum = resp.readEntity(new GenericType<List<Album>>() {
			});
			for (int i = 0; i < listAlbum.size(); i++) {
				if (listAlbum.get(i).getId().equals("77")) {
					System.out.println(listAlbum.get(i).getId() + " - " + listAlbum.get(i).getUserId() + " - "
							+ listAlbum.get(i).getTitle());
				}
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return listAlbum;
	}

	@Override
	public List<Foto> getFotoList() {
		Client client = ClientBuilder.newClient();
		List<Foto> listFoto = new ArrayList<Foto>();
		List<String> listnom = new ArrayList<String>();
 


		try {
			Response resp = client.target("https://jsonplaceholder.typicode.com/photos").request()
					.accept(MediaType.APPLICATION_JSON).get();
			listFoto = resp.readEntity(new GenericType<List<Foto>>() {});
			
			
			//listFoto = listFoto.stream().filter(x-> x.getTitle().equals("ipsam do")).collect(Collectors.toList());
			for (int i = 0; i < listFoto.size(); i++) {
				if (listFoto.get(i).getTitle().contains("ipsam do")) {
					
					listnom.add(listFoto.get(i).getTitle());
					
				}
			//	System.out.println(listFoto.get(i).getTitle());
			}
			Collections.sort(listnom);
			for(String temp: listnom){
			    System.out.println(temp);
			}
			

		} catch (Exception e) {
			System.out.println(e);
		}
		return listFoto;
	}

	
}
