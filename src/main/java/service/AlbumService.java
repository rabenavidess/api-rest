package service;

import java.util.List;

import dto.Album;
import dto.Foto;

public interface AlbumService {

	public List<Album> getAlbumList();
	
	public List<Album> getAlbum();
	
	public List<Foto> getFotoList();
}
