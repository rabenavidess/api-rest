package service;

import java.util.List;

import dto.Usuario;

public interface UsuarioService {

	public List<Usuario> getListUsuarios();
	
	public List<Usuario> getListUsuarioAlbum();
}
